#!/usr/bin/env bash

ROOT="$(dirname "$(dirname "$(realpath "${BASH_SOURCE[0]}")")")"
: "${VIRTUALENV:=$ROOT/.env}"
: "${VIRTUALENV_PROMPT:=(expenses-monitor)}"


[ -d "$VIRTUALENV" ] || virtualenv "$VIRTUALENV" --prompt "$VIRTUALENV_PROMPT"

if [ -z "$VIRTUAL_ENV" ]
then
    source "$VIRTUALENV/bin/activate"
fi

pip install --upgrade pip
pip install -r "$ROOT/scripts/requirements.pip"
