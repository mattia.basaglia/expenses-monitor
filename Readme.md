Expenses Monitor
================

This is a Django-based website to keep track of expenses.


License
-------

AGPLv3+, see COPYING


Installation
------------

First, ensure the requirements listed in scripts/requirements.pip are satisfied
The script `scripts/setup.sh` will create a virtual environment for this.

You'll need to create `src/settings/settings.py` with installation-specific settings.


Running
-------

A development server can be started with `src/manage.pt runserver`,
a WSGI script is provided in `src/wsgi.py`.
Please refer to the Django and your web server documentation for more details.


Sources
-------

See https://gitlab.com/mattia.basaglia/expenses-monitor


Author
------

Mattia Basaglia <mattia.basaglia@gmail.com>
