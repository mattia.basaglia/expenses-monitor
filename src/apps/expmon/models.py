"""
\file

\author Mattia Basaglia

\copyright Copyright 2017 Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone


def AmountField(**kwargs):
    kwargs.setdefault("decimal_places", 2)
    kwargs.setdefault("max_digits", kwargs["decimal_places"] + 9)
    return models.DecimalField(**kwargs)


def get_date():
    """
    For some reason migrations don't like lambdas
    """
    return timezone.now().date


class ExpenseType(models.Model):
    """
    Type of expense defined by the user
    """
    user = models.ForeignKey(User)
    name = models.CharField(max_length=32)
    description = models.CharField(max_length=512)
    sort = models.PositiveSmallIntegerField()

    def __unicode__(self):
        return self.name


class Expense(models.Model):
    """
    An expense item
    """
    type = models.ForeignKey(ExpenseType)
    date = models.DateTimeField(default=timezone.now)
    amount = AmountField()
    note = models.CharField(max_length=512, null=True, blank=True)


class Salary(models.Model):
    """
    Monthly salary, valid after the given date
    """
    user = models.ForeignKey(User)
    date = models.DateField(default=get_date)
    amount = AmountField()


class FixedBalance(models.Model):
    """
    A known fixed value for the user's account, retrieved from date
    """
    user = models.ForeignKey(User)
    date = models.DateField(default=get_date)
    amount = AmountField()
