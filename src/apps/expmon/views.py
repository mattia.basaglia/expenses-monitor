"""
\file

\author Mattia Basaglia

\copyright Copyright 2017 Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from __future__ import unicode_literals
from __future__ import absolute_import


from django.urls import reverse_lazy, reverse
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login, logout
from django.http import HttpResponseRedirect
from django.utils.http import is_safe_url
from django.contrib.auth.decorators import login_required
from django.contrib import messages

from ..better_views.page import Page, LinkGroup, Link, Resource, ViewRegistry
from . import forms, models


views = ViewRegistry()

class ExpMonPage(Page):
    """
    Page with site-specific defaults
    """
    site_name = "Expenses Monitor"

    def __init__(self):
        super(ExpMonPage, self).__init__()

        if not self.block_contents:
            self.block_contents = "expmon/%s.html" % self.slug()

        self.menu = LinkGroup(self.site_name, [
            Home.link(),
            Configure.link(),
            EditExpenses.link(),

            Link(reverse_lazy("admin:index"), "Admin", lambda r: r.user.is_staff),
            Login.link(condition=lambda r: not r.user.is_authenticated),
            Logout.link(condition=lambda r: r.user.is_authenticated),
        ])

        self.footer = [
            LinkGroup(self.site_name, [
                Home.link(),
            ]),
            LinkGroup("Sources", [
                Link("https://gitlab.com/mattia.basaglia/expenses-monitor", self.site_name),
            ]),
        ]


class UserPage(ExpMonPage):
    decorators = [login_required]


@views.register(r'^login/$')
class Login(ExpMonPage):
    def reply(self, request):
        form = AuthenticationForm(request, request.POST or None)
        if form.is_valid():
            login(request, form.get_user())
            default_url = Home.url()
            url = request.GET.get("next", default_url)
            if not is_safe_url(url):
                url = default_url
            return HttpResponseRedirect(url)

        context = {
            "form": form
        }
        return self.render(request, context)


@views.register(r'^logout/$')
class Logout(UserPage):
    def reply(self, request):
        logout(request)
        return HttpResponseRedirect(Login.url())


@views.register(r'^$')
class Home(UserPage):
    def reply(self, request):
        expense_form = forms.ExpenseForm(request.user, request.POST or None)
        if expense_form.is_valid():
            expense_form.save()
            messages.success(request, "Expense recorded")
            expense_form = forms.ExpenseForm(request.user, request.POST or None)
        context = {
            "expense_form": expense_form,
        }
        return self.render(request, context)


@views.register(r'^configure/$')
class Configure(UserPage):
    resources = UserPage.resources + [
        Resource(Resource.Script|Resource.Url, "https://code.jquery.com/jquery-3.2.1.min.js"),
        Resource(Resource.Script|Resource.Static, "scripts/formset.js"),
    ]

    def reply(self, request):
        formset = self.make_formset(request)
        if formset.is_valid():
            formset.save()
            messages.success(request, "Expense types updated")
            formset = self.make_formset(request, False)
        context = {
            "formset": formset,
        }
        return self.render(request, context)

    def make_formset(self, request, withdata=True):
        return forms.ExpenseTypeFormSet(
            request.POST or None if withdata else None,
            form_kwargs={"user": request.user},
            queryset=request.user.expensetype_set.order_by('sort'),
        )


@views.register(r'^expenses/edit$')
class EditExpenses(UserPage):
    def reply(self, request):
        filter_form = forms.HistoricalExpenseFilter(request.POST or None)
        queryset = models.Expense.objects.filter(type__user_id=request.user.id)
        if filter_form.is_valid():
            queryset = filter_form.get_queryset()

        formset = forms.HistoricalExpenseFormSet(
            request.user,
            request.POST or None,
            queryset=queryset,
        )
        if formset.is_valid():
            formset.save()
            messages.success(request, "Expense recorded")
            formset = forms.ExpenseForm(request.user, request.POST or None)
        context = {
            "formset": formset,
        }
        return self.render(request, context)

