"""
\file

\author Mattia Basaglia

\copyright Copyright 2017 Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import datetime

from dateutils import relativedelta

from django import forms
from django.utils import timezone

from . import models


class RelatedToUserForm(forms.ModelForm):
    def __init__(self, user, *args, **kwargs):
        super(RelatedToUserForm, self).__init__(*args, **kwargs)
        self.user = user

    def save(self, commit=True):
        object = super(RelatedToUserForm, self).save(False)
        object.user = self.user
        if commit:
            object.save()
        return object


class OrderedFormset(forms.BaseModelFormSet):
    order_field = 'sort'
    def save(self, commit=True):
        for form in self.ordered_forms:
            setattr(form.instance, self.order_field, form.cleaned_data['ORDER'])
        return super(OrderedFormset, self).save(commit)


ExpenseTypeFormSet = forms.modelformset_factory(
    models.ExpenseType,
    RelatedToUserForm,
    formset=OrderedFormset,
    fields=['name', 'description'],
    can_delete=True,
    can_order=True,
    extra=0,
)


ExpenseForm = forms.modelform_factory(
    models.Expense,
    RelatedToUserForm,
    ['amount', 'type', 'note']
)


HistoricalExpenseFormSet = forms.modelformset_factory(
    models.Expense,
    RelatedToUserForm,
    fields=['date', 'amount', 'type', 'note'],
    extra=3,
    can_delete=True,
)


def this_month():
    today = timezone.now()
    return datetime.date(today.year, today.month, 1)


def end_this_month():
    return this_month() + relativedelta(months=+1, days=-1)


class HistoricalExpenseFilter(forms.Form):
    date_from = forms.DateField(
        initial=this_month
    )
    date_until = forms.DateField(
        initial=end_this_month
    )

    def get_queryset(self, queryset):
        if queryset is None:
            queryset = models.Expense.objects.all()
        return queryset.filter(
            date__gte=self.cleaned_data["date_from"],
            date__lt=self.cleaned_data["date_until"]
        )
