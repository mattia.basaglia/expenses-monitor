/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright 2017 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \brief Adds a click event to order buttons that moves their parent
 */
function setup_move_buttons(parent)
{
    $(parent).find("button[data-type='move']").click(function(){
        var button = $(this);
        var target = $('#' + button.data("target"));
        var direction = Number(button.data("direction"));
        var siblings = target.parent().children();
        var other = siblings[
            Math.min(
                Math.max(
                    Number(button.data("min")) || 0,
                    target.index() + direction
                ),
                Number(button.data("max")) || siblings.length - 1
            )
        ];

        if ( other == parent[0] )
            return;

        if ( direction < 0 )
        {
            target.insertBefore(other);
        }
        else
        {
            target.insertAfter(other);
        }
    });
}

/**
 * \brief Updates order input values based on their parent's position
 */
function update_order_input(parent)
{
    parent.find("input[data-order-from]").each(function(){
        var input = $(this);
        var parent = $('#' + input.data("order-from"));
        input.val(parent.index());
    });
}

/**
 * \brief Adds events to hide/show the parent when a delete/restore button is clicked
 */
function setup_delete_buttons(parent)
{
    var toggle_delete = function(button, remove)
    {
        var button = $(button);
        var target = $('#' + button.data("target"));
        var input = $('#' + button.data("input"));

        input.val(remove ? '1' : '');

        target.toggle(!remove);

        if ( button.data("toggle") != undefined )
        {
            var toggle = $('#' + button.data("toggle"));
            toggle.toggle(remove);
        }
    };

    $(parent).find("button[data-type='delete']")
        .click(function(){toggle_delete(this, true);})
        .each(function(){toggle_delete(this, false);})
    ;
    $(parent).find("button[data-type='undelete']")
        .click(function(){toggle_delete(this, false);})
    ;
}

/**
 * \brief Sets up all formset buttons for form management inside a formset
 */
function setup_formset_form_buttons(parent)
{
    setup_move_buttons(parent);
    setup_delete_buttons(parent);
}

function setup_add_button(parent)
{
    var button = $(parent).find("button[data-type='add']")
    var template = $('#' + button.data("template"));
    var parent = $(button.data("parent"));
    var name_prefix = button.data("prefix") + '-';
    var id_prefix = "id_" + name_prefix;
    var total_forms = $('#' + id_prefix + 'TOTAL_FORMS');
    var max_forms = $('#' + id_prefix + 'MAX_NUM_FORMS');

    var hide_if_max = function()
    {
        if ( parseFloat(total_forms.val()) >= parseFloat(max_forms.val()) )
            button.hide();
    };

    button.click(function(){
        var order_input_selector = 'input[type="hidden"]'
            + '[name^="'+name_prefix+'"][name$="-ORDER"]';
        var form_order_list = parent.find(order_input_selector)
            .map(function() {return parseFloat(this.value);})
            .get();
        var form_order = Math.max.apply(form_order_list) + 1;

        var form_id = parseFloat(total_forms.val());
        total_forms.val(form_id+1);

        var element = $($(template).html().replace(/__prefix__/g, form_id));
        $(parent).append(element);
        setup_formset_form_buttons(element);
        element.find(order_input_selector).val(form_order);

        hide_if_max();
    });
    hide_if_max();
}

/**
 * \brief Sets up all dynamic actions in a formset
 */
function setup_formset(formset)
{
    var formset = $(formset);
    update_order_input(formset);
    formset.submit(function(){update_order_input(formset)});
    setup_formset_form_buttons(formset);
    setup_add_button(formset);
}
