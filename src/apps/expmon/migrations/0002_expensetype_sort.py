# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-05-06 08:11
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('expmon', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='expensetype',
            name='sort',
            field=models.PositiveSmallIntegerField(default=0),
            preserve_default=False,
        ),
    ]
