"""
\file

\author Mattia Basaglia

\copyright Copyright 2017 Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import os

def activate():
    if "VIRTUAL_ENV" not in os.environ:
        activate_file = os.path.join(os.path.dirname(os.path.dirname(__file__)), ".env", "bin", "activate_this.py")
        if os.path.exists(activate_file):
            #exec(open(activate_file).read())
            execfile(activate_file, dict(__file__=activate_file))
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings.settings")
